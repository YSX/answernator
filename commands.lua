local function bigMessage( Ttable, message, header, symb )
  symb = symb or ""
  header = header and header.."\n" or "\n"
  local msg = ""

  for _, messageString in pairs(Ttable) do
    if string.utf8len(header..symb..msg.."\n"..messageString..symb) > 2000 then
      if msg ~= "" then
        sendAndDelete(message.channel, header..symb..msg..symb, serverdata[message.server.id].deleteDelay)
        header = ""
      end
      msg = messageString
      while string.utf8len(header..symb..msg..symb) > 2000 do
        sendAndDelete(message.channel, header..symb..string.utf8sub(msg, 1, 2000-string.utf8len(header)-(string.utf8len(symb)*2))..symb, serverdata[message.server.id].deleteDelay)
        msg = string.utf8sub(msg, 2001-string.utf8len(header)-(string.utf8len(symb)*2), -1)
      end
    else
      msg = (msg == "" and messageString) or msg.."\n"..messageString
    end
  end
  sendAndDelete(message.channel, header..symb..msg..symb, serverdata[message.server.id].deleteDelay)
end

local function secToTime(numSec)
  local coolTime
  local nSeconds = numSec
  if nSeconds == 0 then
    coolTime = "00:00:00"
  else
    local nHours = string.format("%02.f", math.floor(nSeconds/3600))
    local nMins = string.format("%02.f", math.floor(nSeconds/60 - (nHours*60)))
    local nSecs = string.format("%02.f", math.floor(nSeconds - nHours*3600 - nMins *60))
    coolTime =  nHours..":"..nMins..":"..nSecs
  end
  return coolTime
end

local function checkWhitelist(Tmember, Tserver)
  for _, role in pairs(Tmember.roles) do
    if serverdata[Tserver.id].whitelistedRoles[role.id] == true then return true 
    elseif string.utf8lower(role.name) == config.whitelistedRole then return true end
  end
  if checkPerm(Tmember, Tserver) then return true end
  return false
end

--[[
commands["command"] = {
  help = "string", -- name of help string in texts[lang].help[string]
  langsWhitelist = {"strings"}, -- only if I want to use only X langs

  group = "user" or "admin" or "owner" or "permission", -- who can access the command, permission - depends on member perms, not on role
  perm = "somePermission" or nil -- if group = "permission", we need a perm to check
  whitelist = {"serverID"} or nil, -- table of server IDs, works as whitelist for exec cmd on servers
  blacklist = {"serverID"} or nil, -- table of server IDs, blacklists this servers
  members = {"memberID"} or nil, -- if exists, only this users will be able to use the command
  split = true or false or number, -- split to cmd and arg or split args word-by-word or just to N pieces?
  allowMultiple = true or nil, -- if true, user can spam this command
  beta = true or nil, -- if true, command can be used without help string

  func = function( message, arg )
    -- body of function
  end
}
]]
local commands = {}

commands["help"] = {
  help = "help",

  group = "user",
  split = false,

  func = function(message, args)
    local answer = {}
    for cmd, massiv in pairs(commands) do
      local willAdd = true

      if massiv.func == nil then willAdd = false end
      if commands[cmd].disabled == true then willAdd = false end

      if massiv[serverdata[message.server.id].lang] == nil or massiv[serverdata[message.server.id].lang] == "" then willAdd = false end

      if commands[cmd].group == "admin" then
        if checkPerm(message.author, message.server) == false then willAdd = false end
      elseif commands[cmd].group == "owner" then
        if message.author.id ~= config.authorID then willAdd = false end
      elseif commands[cmd].group == "permission" then
        if commands[cmd].perm then
          if not hasPermission(message.author, commands[cmd].perm) then willAdd = false end
        else
          writeLog("No permission set for command "..cmd, "ERR")
          willAdd = false
        end
      end

      if commands[cmd].whitelist then
        local answer = false
        for _, servName in pairs(commands[cmd].whitelist) do
          if message.server.id == servName then answer = true;break end
        end
        if answer == false then willAdd = false end
      end

      if commands[cmd].blacklist then
        for _, servName in pairs(commands[cmd].blacklist) do
          if message.server.id == servName then willAdd = false end
        end
      end

      if commands[cmd].members then
        local answer = false
        for _, membName in pairs(commands[cmd].members) do
          if message.author.id == membName then answer = true;break end
        end
        if answer == false then willAdd = false end
      end

      if willAdd == true then table.insert(answer, "`"..config.commandSymbol..cmd.."` - "..massiv[serverdata[message.server.id].lang]) end
    end

    table.insert(answer, texts[serverdata[message.server.id].lang].commands.cmdlist2)
    bigMessage(answer, message, string.format(texts[serverdata[message.server.id].lang].commands.cmdlist, message.author:getMentionString()))
  end
}

commands["random"] = {
  help = "random",

  group = "user",
  split = true,

  func = function(message, args)
    local answer
    --math.randomseed(os.time())
    if #args == 1 then
      answer = message.author:getMentionString()..", "..math.random()
    elseif #args == 2 and type(tonumber(args[2])) == type(1) then
      answer = message.author:getMentionString()..", "..math.random(args[2])
    elseif #args == 3 and type(tonumber(args[2])) == type(1) and type(tonumber(args[3])) == type(1) then
      answer = message.author:getMentionString()..", "..math.random(args[2], args[3])
    else
      answer = string.format(texts[serverdata[message.server.id].lang].commands.random, message.author:getMentionString())
    end
    sendAndDelete(message.channel, answer, serverdata[message.server.id].deleteDelay)
  end
}

commands["вк"] = {
  help = "vk",
  langsWhitelist = {"ru"},

  group = "user",
  split = false,

  func = function(message)
    if fs.existsSync(makePath("serverdata/"..message.server.id.."/vk.txt"))==true then
      p(true)
      sendAndDelete(message.channel, fs.readFileSync(makePath("serverdata/"..message.server.id.."/vk.txt")), serverdata[message.server.id].deleteDelay)
    else
      p(false)
      sendAndDelete(message.channel, texts[serverdata[message.server.id].lang].commands.noTextSet, serverdata[message.server.id].deleteDelay)
    end
  end
}

commands["facebook"] = {
  help = "facebook",

  group = "user",
  split = false,

  func = function(message)
    if fs.existsSync(makePath("serverdata/"..message.server.id.."/fb.txt"))==true then
      sendAndDelete(message.channel, fs.readFileSync(makePath("serverdata/"..message.server.id.."/fb.txt")), serverdata[message.server.id].deleteDelay)
    else
      sendAndDelete(message.channel, texts[serverdata[message.server.id].lang].commands.noTextSet, serverdata[message.server.id].deleteDelay)
    end
  end
}

commands["ссылки"] = {
  help = "links",
  langsWhitelist = {"ru"},

  group = "user",
  split = false,

  func = function(message)
    if fs.existsSync(makePath("serverdata/"..message.server.id.."/links.txt"))==true then
      sendAndDelete(message.channel, fs.readFileSync(makePath("serverdata/"..message.server.id.."/links.txt")), serverdata[message.server.id].deleteDelay)
    else
      sendAndDelete(message.channel, texts[serverdata[message.server.id].lang].commands.noTextSet, serverdata[message.server.id].deleteDelay)
    end
  end
}

commands["invite"] = {
  help = "invite",

  group = "user",
  split = false,

  func = function(message)
    sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.invite, message.author:getMentionString()), serverdata[message.server.id].deleteDelay)
  end
}

commands["userinfo"] = {
  help = "userinfo",

  group = "user",
  split = false,

  func = function(message)
    local answer = {}
    local count = texts[serverdata[message.server.id].lang].commands.idWrong
    for id,name in pairs(message.mentions.members) do
      table.insert(answer, "**"..name.username..":**")
      table.insert(answer, "ID: "..name.id)
      table.insert(answer, string.format(texts[serverdata[message.server.id].lang].commands.userinfo.joinedAt, os.date("%d.%m.%y %X", tonumber(name.joinedAt))))
      local Ttime = os.time()
      local Tmounths = math.floor( (Ttime - tonumber(name.joinedAt))/2592000 )
      local Tdays = math.floor((Ttime-(Tmounths*2592000)-tonumber(name.joinedAt))/86400)
      table.insert(answer, string.format(texts[serverdata[message.server.id].lang].commands.userinfo.time, Tmounths, Tdays))
      if count ~= "" then count = "" end
      for _,roleName in pairs(name.roles) do
        table.insert(answer, roleName.name..", Role ID: "..roleName.id)
      end
    end
    for _, name in pairs(message.mentions.roles) do
      if count ~= "" then count = "" end
      table.insert(answer, "Role: "..name.name..", id "..name.id)
    end
    bigMessage(answer, message, message.author:getMentionString()..","..count)
  end
}

commands["banlist"] = {
  help = "banlist",

  group = "user",
  split = false,

  func = function(message)
    -- Checking perms
    local hasPermission = false
    for _, role in pairs(message.server.me.roles) do
      if role.permissions:hasPermission("banMembers") or role.permissions:hasPermission("administrator") then hasPermission = true end
    end

    if hasPermission == false then
      writeLog("No access to banlist", "WARN")
      sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.banlistAccess, message.author:getMentionString()), serverdata[message.server.id].deleteDelay)
      return
    end
    -- --
    local banned = 0
    local answer = {}
    local banlist = message.server:getBannedUsers()
    for _,v in pairs(banlist) do
      table.insert(answer, "**"..v.username.."** (ID: "..v.id..")")
      banned = banned +1
    end
    bigMessage(answer, message, string.format(texts[serverdata[message.server.id].lang].commands.banlist, message.author:getMentionString(), banned))
  end
}

commands["vote"] = {
  help = "vote",

  group = "user",
  split = 3,

  func = function(message, args)
    if args[2] == "kick" or args[2] == "ban" then
      if table.size(message.mentions.members, true) == 0 then
        sendAndDelete(message.channel, string.format(texts[config[message.server.id].lang].commands.vote.nomentions, message.author:getMentionString()), serverdata[message.server.id].deleteDelay)
      end
      local answer = {}
      if server[message.server.id].voteLimits[args[2]] then
        if server[message.server.id].voteLimits[args[2]] > 0 then
          votedata[message.server.id] = votedata[message.server.id] or {kick={},ban={}}
          for _, member in pairs(message.mentions.members) do
            if checkWhitelist(member, message.server) == false then
              votedata[message.server.id][args[2]][member.id] = votedata[message.server.id][args[2]][member.id] or {}
              if tableFind(votedata[message.server.id][args[2]][member.id], message.author.id) == false then
                table.insert(votedata[message.server.id][args[2]][member.id], message.author.id)
              end
              if #votedata[message.server.id][args[2]][member.id] >= server[message.server.id].voteLimits[args[2]] then
                if args[2] == "kick" then
                  message.server:kickUser(member)
                  table.insert(answer, string.format(texts[config[message.server.id].lang].commands.vote.kicked, message.author:getMentionString(), member:getMentionString()))
                elseif args[2] == "ban" then
                  message.server:banUser(member)
                  table.insert(answer, string.format(texts[config[message.server.id].lang].commands.vote.banned, message.author:getMentionString(), member:getMentionString()))
                end
              else
                if args[2] == "kick" then
                  table.insert(answer, string.format(texts[config[message.server.id].lang].commands.vote.kickAdded, message.author:getMentionString(), member:getMentionString(), server[message.server.id].voteLimits[args[2]]-#votedata[message.server.id][args[2]][member.id]))
                elseif args[2] == "ban" then
                  table.insert(answer, string.format(texts[config[message.server.id].lang].commands.vote.banAdded, message.author:getMentionString(), member:getMentionString(), server[message.server.id].voteLimits[args[2]]-#votedata[message.server.id][args[2]][member.id]))
                end
              end
            end
          end
        else
          sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.vote.disabled, message.author:getMentionString()), serverdata[message.server.id].deleteDelay)
        end
      else
        sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.vote.nolimits, message.author:getMentionString()), serverdata[message.server.id].deleteDelay)
        return
      end
      bigMessage(answer, message, message.author:getMentionString())
      local file = io.open(makePath("serverdata/"..message.server.id.."/votes.json"), "w")
      file:write(json.stringify(votedata[message.server.id]))
      file:close()
    else
      sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.vote.usage, message.author:getMentionString()), serverdata[message.server.id].deleteDelay)
    end
  end
}

commands["magicball"] = {
  help = "magicball",

  group = "user",
  split = false,

  func = function(message, args)
    if args == nil then
      sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.magicBall.noquestion, message.author:getMentionString()), serverdata[message.server.id].deleteDelay)
    else
      commandsData.timeouts.magicball = commandsData.timeouts.magicball or {}
      commandsData.timeouts.magicball[message.server.id] = commandsData.timeouts.magicball[message.server.id] or {}
      commandsData.timeouts.magicball[message.server.id][message.author.id] = commandsData.timeouts.magicball[message.server.id][message.author.id] or 0
      if os.time()-commandsData.timeouts.magicball[message.server.id][message.author.id]<300 then
        sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.magicBall.timeout, message.author:getMentionString(), secToTime(300-(os.time()-timeouts.magicball[message.server.id][message.author.id]))), serverdata[message.server.id].deleteDelay)
      else
        commandsData.timeouts.magicball[message.server.id][message.author.id] = os.time()
        sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.magicBall.answer, message.author:getMentionString(), args, string.format(selectRandom(texts[serverdata[message.server.id].lang].commands.magicBallAnswers), message.author:getMentionString())), serverdata[message.server.id].deleteDelay)
      end
    end
  end
}

commands["царь"] = {
  help = "tsar",
  langsWhitelist = {"ru"},

  group = "user",
  split = true,
  members = {config.authorID, "191647945362374657"},

  func = function(message, args)
    args[2] = args[2] or "help"
    if args[2] == "help" then
      sendAndDelete(message.channel, string.format(texts.ru.user.tsar.usage, message.author:getMentionString()), serverdata[message.server.id].deleteDelay)
    elseif args[2] == "велит" then
      if table.size(message.mentions.members, true) ~= 1 then whomEbat = "васЪ" else
        for _, ment in pairs(message.mentions.members) do whomEbat = ment:getMentionString();break end
      end
      local answer = {}
      table.insert(answer, string.format(selectRandom(texts.ru.user.tsarAnswers), whomEbat))
      bigMessage(answer, message, string.format(texts.ru.user.tsar.header, message.author:getMentionString()))
    end
  end
}

commands["debug"] = {
  help = "debug",

  group = "owner",
  split = false,

  func = function(message, arg)
    local answer = {}
    local function addToAnswer( header, someTable )
      table.insert(answer, header)
      for key, value in pairs(someTable) do
        table.insert(answer, key..": type "..type(value)..", "..tostring(value))
      end
    end
    local function addToAnswer2( header, someTable )
      table.insert(answer, header)
      for key,value in pairs(someTable) do
        table.insert(answer, key..": type "..type(value)..", "..tostring(value))
        if type(value) == "table" then
          for key2,value2 in pairs(value) do
            table.insert(answer, key.."||"..key2..": type "..type(value2)..", "..tostring(value2))
          end
        end
      end
    end

    addToAnswer("*message*\n", message)
    addToAnswer("\n*message.author*\n", message.author)
    addToAnswer("\n*client.handlers*\n", client.handlers)
    addToAnswer("\n*message.server*\n", message.server)
    addToAnswer("\n*message.server.me*\n", message.server.me)
    addToAnswer("\n*message.server.owner*\n", message.server.owner)
    addToAnswer("\n*client.servers*\n", client.servers)

    addToAnswer2("\n*message.mentions.members*\n", message.mentions.members)
    addToAnswer2("\n*message.author.roles*\n", message.author.roles)

    bigMessage(answer, message, "**DEBUG INFO:**")
  end
}

commands["rename"] = {
  help = "rename",

  group = "owner",
  split = false,

  func = function(message, arg)
    if arg == nil or arg == "" then return end
    client:setUsername(arg, "smthHere")
    writeLog("Changed name to "..arg, "WARN")
    sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.rename..arg, message.author:getMentionString()), serverdata[message.server.id].deleteDelay)
  end
}

commands["clear"] = {
  help = "clear",

  group = "admin",
  split = true,

  func = function(message, args)
    if args[2] == "messages" then
      if tonumber(args[3]) == nil then return end
      local messages = message.channel:getMessageHistory(args[3])
      message.channel:bulkDelete(messages)
      sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.clear.cleared, message.author:getMentionString()), serverdata[message.server.id].deleteDelay)
      return true
    elseif args[2]=="servervotes" then
      votedata[message.server.id]=nil
      fs.unlinkSync(makePath("serverdata/"..message.server.id.."/votes.json"))
      sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.clear.deleted, message.author:getMentionString()), serverdata[message.server.id].deleteDelay)
    else
      sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.clear.default, message.author:getMentionString()), serverdata[message.server.id].deleteDelay)
    end
  end
}

commands["stop"] = {
  help = "stop",

  group = "owner",
  split = false,

  func = function(message, arg)
    message.channel:sendMessage(string.format(texts[serverdata[message.server.id].lang].commands.stop, message.author:getMentionString()))
    writeLog("Stop command received, stopping bot", "WARN")
    message:delete()
    client:stop()
  end
}

commands["статус"] = {
  help = "status",
  langsWhitelist = {"ru"},

  group = "owner",
  split = false,

  func = function(message, arg)
    if arg == nil or arg == "" then
      client:setStatusOnline()
      sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.status.online, message.author:getMentionString()), serverdata[message.server.id].deleteDelay)
      writeLog("Changed status to: Online")
    elseif arg == "randomgame" then
      --math.randomseed(os.time())
      arg = getGameName("./assets/video_game_names.txt", "random")
      client:setGameName(arg)
      sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.status.playing ..'*' .. arg .. '*', message.author:getMentionString()), serverdata[message.server.id].deleteDelay)
      writeLog("Changed status to: " .. arg)
    elseif string.find( arg, "^randomgame start" ) then
      local Tdelay = string.match(arg, 'randomgame start ([%d]+)')
      Tdelay = tonumber(Tdelay or 30) or 30
      Tdelay = Tdelay*60000
      if gameTimer then
        writeLog("Already set autostatus.", "WARN")
        sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.status.timerExists, message.author:getMentionString()), serverdata[message.server.id].deleteDelay)
      else
        writeLog("Started autostatus. Delay: "..tostring(Tdelay/60000).." min")
        _G.gameTimer = timer.setInterval(Tdelay, function()
          --math.randomseed(os.time())
          AUTarg = getGameName("./assets/video_game_names.txt", "random")
          coroutine.wrap(client.setGameName)(client, AUTarg)
          writeLog("Autochanged status to: " .. AUTarg)
        end)
        --math.randomseed(os.time())
        arg = getGameName("./assets/video_game_names.txt", "random")
        client:setGameName(arg)
        writeLog("Changed status to: " .. arg)
        sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.status.timerSet, message.author:getMentionString(), Tdelay/60000), serverdata[message.server.id].deleteDelay)
      end
    elseif arg == "randomgame stop" then
      if gameTimer then
        timer.clearInterval(gameTimer)
        _G.gameTimer = nil
        writeLog("Autochange status stopped")
        sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.status.timerRemoved, message.author:getMentionString()), serverdata[message.server.id].deleteDelay)
      else
        writeLog("Autochange status not stopped: no timer exists")
        sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.status.timerNotExists, message.author:getMentionString()), serverdata[message.server.id].deleteDelay)
      end
    elseif arg == "help" then
      sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.status.help, message.author:getMentionString()), serverdata[message.server.id].deleteDelay)      
    elseif arg == "idle" or arg == "отошел" then
      client:setStatusIdle()
      sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.status.idle, message.author:getMentionString()), serverdata[message.server.id].deleteDelay)
      writeLog("Changed status to: IDLE")
    else
      client:setGameName(arg)
      sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.status.playing ..'*' .. arg .. '*', message.author:getMentionString()), serverdata[message.server.id].deleteDelay)
      writeLog("Changed status to: " .. arg)
    end
  end
}

commands["фразы"] = {
  help = "phrases",
  langsWhitelist = {"ru"},

  group = "admin",
  split = false,

  func = function(message, arg)
    local answer = {}
    for phraseName, phrase in pairs(texts[serverdata[message.server.id].lang].phrases) do
      if phrase.triggers then
        if phrase.name then
          table.insert(answer, "\n**__"..phrase.name.."__**:")
        else
          table.insert(answer, "\n**__"..phraseName.."__**:")
        end
        table.insert(answer, texts[serverdata[message.server.id].lang].commands.phrases.triggers)
        for _,triggerTexts in pairs(phrase.triggers) do
          table.insert(answer, "  "..table.tostring(triggerTexts))
        end
        table.insert(answer, texts[serverdata[message.server.id].lang].commands.phrases.phrases)
        if phrase.answers then
          table.insert(answer, texts[serverdata[message.server.id].lang].commands.phrases.answers)
          for _,phraseText in pairs(phrase.answers) do
            table.insert(answer, "  "..phraseText)
          end
        end
        if phrase.temperAnswers then
          texts[serverdata[message.server.id].lang].commands.temper.tempers = texts[serverdata[message.server.id].lang].commands.temper.tempers or {}
          table.insert(answer, texts[serverdata[message.server.id].lang].commands.phrases.temperAnswers)
          for temper, phraseTemper in pairs(phrase.temperAnswers) do
            table.insert(answer, "***__"..temper..":__***")
            for _,phraseText in pairs(phraseTemper) do
              table.insert(answer, "  "..phraseText)
            end
          end
        end
        if phrase.temperBonus then
          table.insert(answer, string.format(texts[serverdata[message.server.id].lang].commands.phrases.temperBonus, phrase.temperBonus))
        end
        if phrase.exceptions then
          table.insert(answer, texts[serverdata[message.server.id].lang].commands.phrases.exceptions)
          table.insert(answer, "  "..table.tostring(phrase.exceptions))
        end
      end
    end
    if #answer == 0 then
      table.insert(answer, texts[serverdata[message.server.id].lang].commands.phrases.nophrases)
    end
    bigMessage(answer, message, string.format(texts[serverdata[message.server.id].lang].commands.phrases.header, message.author:getMentionString()))
  end
}

commands["console"] = {
  help = "console",

  group = "owner",
  split = false,

  func = function(message, arg)
    writeLog(arg, "TO CONSOLE")
  end
}

commands["todo"] = {
  help = "todo",

  group = "owner",
  split = false,

  func = function(message, arg)
    local todoFile = "./assets/TODO.md"
    local args = {}
    args[1], args[2] = string.match(arg or "", '(%S+) (.*)')
    arg = nil
    if args[1] then args[1] = string.utf8lower(args[1]) end
    if args[1] == "add" then
      -- дописываем в todo.md
      if args[2] == nil or args[2] == "" then args[2] = "ERROR: NO ARG" else
        local file = io.open(todoFile, "a")
        if file == nil then writeLog("Cannot open todo.md", "ERR");return end
        file:write(args[2].."\n")
        file:close()
        sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.todo.add, message.author:getMentionString(), args[2]), serverdata[message.server.id].deleteDelay)
      end
    elseif args[1] == "del" or args[1] == "rm" or args[1] == "remove" then
      if tonumber(args[2]) == nil then sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.todo.delWrong, message.author:getMentionString()), serverdata[message.server.id].deleteDelay) else
        local file = io.open(todoFile, "r")
        if file == nil then writeLog("Cannot open todo.md", "ERR");return end
        local answer = {}
        for TDstring in file:lines() do
          table.insert(answer, TDstring)
        end
        file:close()
        table.remove(answer, tonumber(args[2]))
        local file = io.open(todoFile, "w")
        if file == nil then writeLog("Cannot open todo.md to write", "ERR");return end
        for i=1,#answer do
          file:write(answer[i].."\n")
        end
        file:close()
        sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.todo.del, message.author:getMentionString(), args[2]), serverdata[message.server.id].deleteDelay)
      end
    else
      -- вывод TODO.md
      local file = io.open(todoFile, "r")
      if file == nil then writeLog("Cannot open todo.md", "ERR");return end
      local answer = {}
      local num = 1
      for TDstring in file:lines() do
        table.insert(answer, " "..num..". "..TDstring)
        num = num+1
      end
      file:close()
      bigMessage(answer, message, message.author:getMentionString()..", TODO:")
    end
  end
}

commands["servers"] = {
  help = "servers",

  group = "owner",
  split = false,
  disabled = false,

  func = function(message, arg)
    local answer = {}
    for _,Serv in pairs(client.servers) do
      table.insert(answer, Serv.name..", ID: "..Serv.id)
    end
    bigMessage(answer, message, message.author:getMentionString())
  end
}

commands["kick"] = {
  help = "kick",

  group = "permission",
  perm = "kickMembers",
  split = false,

  func = function(message, arg)
    local answer = {}
    for _, member in pairs(message.mentions.members) do
      if checkWhitelist(member, message.server) then
        table.insert(answer, string.format(texts[serverdata[message.server.id].lang].commands.kick.accessDenied, member:getMentionString()))
        writeLog(string.format("Can't kick user %s (ID: %s). Who tried: %s (ID: %s) on server %s (ID: %s)", member.name, member.id, message.author.name, message.author.id, message.server.name, message.server.id), "ERR")
      else
        message.server:kickUser(member)
        table.insert(answer, string.format(texts[serverdata[message.server.id].lang].commands.kick.success, member:getMentionString()))
        writeLog(string.format("User %s (ID: %s) kicked by %s (ID: %s) on server %s (ID: %s)", member.name, member.id, message.author.name, message.author.id, message.server.name, message.server.id), "WARN")
      end
    end
    bigMessage(answer, message, message.author:getMentionString())
  end
}

commands["ban"] = {
  help = "ban",

  group = "permission",
  perm = "banMembers",
  split = false,

  func = function(message, arg)
    local answer = {}
    for _, member in pairs(message.mentions.members) do
      if checkWhitelist(member, message.server) then
        table.insert(answer, string.format(texts[serverdata[message.server.id].lang].commands.ban.accessDenied, member:getMentionString()))
        writeLog(string.format("Can't ban user %s (ID: %s). Who tried: %s (ID: %s) on server %s (ID: %s)", member.name, member.id, message.author.name, message.author.id, message.server.name, message.server.id), "ERR")
      else
        message.server:banUser(member)
        table.insert(answer, string.format(texts[serverdata[message.server.id].lang].commands.ban.success, member:getMentionString()))
        writeLog(string.format("User %s (ID: %s) banned by %s (ID: %s) on server %s (ID: %s)", member.name, member.id, message.author.name, message.author.id, message.server.name, message.server.id), "WARN")
      end
    end
    bigMessage(answer, message, message.author:getMentionString())
  end
}

commands["getinvite"] = {
  help = "getinvite",

  group = "owner",
  split = false,

  func = function(message, arg)
    local answer = {}
    if client:getServerById(arg) then
      table.insert(answer, "**"..client:getServerById(arg).name..":**")
      if client:getServerById(arg):getInvites() then
        if table.size(client:getServerById(arg):getInvites(), true) == 0 then
          client:getServerById(arg).defaultChannel:createInvite()
        end
        if table.size(client:getServerById(arg):getInvites(), true) == 0 then
          table.insert(answer, texts[serverdata[message.server.id].lang].commands.getinvite.noInviteAccess)
        else
          local invites = client:getServerById(arg):getInvites()
          for k,v in pairs(invites) do
            table.insert(answer, tostring(v))
          end
        end
      else
        local newInvite = client:getServerById(arg).defaultChannel:createInvite()
        if newInvite then
          table.insert(answer, tostring(newInvite))
        else
          table.insert(answer, texts[serverdata[message.server.id].lang].commands.getinvite.noInviteAccess)
        end
      end
    else
      table.insert(answer, texts[serverdata[message.server.id].lang].commands.getinvite.noserver)
    end
    bigMessage(answer, message, message.author:getMentionString())
  end
}

commands["getroles"] = {
  help = "getroles",

  group = "owner",
  split = false,

  func = function(message, arg)
    local answer = {}
    for TroleID,Trole in pairs(message.server.roles) do
      table.insert(answer, string.format("```\n%s\n```", Trole.name or "Unknown role"))
      local needDataInRole = {"id", "position", "managed", "hoist", "color"}
      for _, neededData in pairs(needDataInRole) do
        table.insert(answer, neededData..": `"..(type(Trole[neededData]) == "table" and table.tostring(Trole[neededData], false, 2, false, 2) or tostring(Trole[neededData])).."`")
      end
      table.insert(answer, "\n```\nRole permissions:\n```")
      local flags = {"createInstantInvite", "kickMembers", "banMembers", "administrator", "manageChannels", "manageGuild", "readMessages", "sendMessages", "sendTTSMessages", "manageMessages", "embedLinks", "attachFiles", "readMessageHistory", "mentionEveryone", "connect", "speak",  "muteMembers", "deafenMembers", "moveMembers", "useVoiceActivity", "changeNickname", "manageNickname", "manageRoles"}
      local TRolePermissions
      for _, flag in pairs(flags) do
        if Trole.permissions:hasPermission(flag) then TRolePermissions = TRolePermissions and TRolePermissions..", `"..flag.."`" or "`"..flag.."`" end
      end
      table.insert(answer, TRolePermissions.."\n")
    end
    bigMessage(answer, message, message.author:getMentionString())
  end
}

commands["leave"] = {
  help = "leave",

  group = "owner",
  split = false,
  disabled = true,

  func = function(message, arg)
    if arg == nil then
      sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.leave.usage, message.author:getMentionString()), serverdata[message.server.id].deleteDelay)
    elseif arg == "this" then
      message.channel:sendMessage(texts[serverdata[message.server.id].lang].commands.leave.leave)
      message.server:leave()
    else
      if client:getServerById(arg) then
        client:getServerById(arg):getChannelById(serverdata[arg].defaultChannel):sendMessage(texts[serverdata[arg].lang].commands.leave.leave)
        client:getServerById(arg):leave()
        writeLog("Leaved server!", "WARN")
      else
        sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.leave.noserver, message.author:getMentionString()), serverdata[message.server.id].deleteDelay)
      end
    end
  end
}

commands["serverinfo"] = {
  help = "serverinfo",

  split = false,
  group = "owner",

  func = function( message, arg )
    if arg then
      if client:getServerById(arg) then
        local answer = {}
        local server = client:getServerById(arg)
        table.insert(answer, "ID: "..server.id)
        table.insert(answer, "Name: "..server.name)
        table.insert(answer, "Owner name: "..server.owner.name.." (ID: "..server.owner.id..")")
        table.insert(answer, "default channel: "..server.defaultChannel.name)
        table.insert(answer, "Members: "..tostring(table.size(server.members, true)))
        bigMessage(answer, message, message.author:getMentionString())
      else
        sendAndDelete(message.channel, "Invalid server ID", serverdata[message.server.id].deleteDelay)
      end
    else
      sendAndDelete(message.channel, "No server ID", serverdata[message.server.id].deleteDelay)
    end
  end
}

commands["lua"] = {
  help = "lua",

  split = false,
  group = "owner",
  disabled = false,

  func = function( message, arg )
    local success, err = pcall(function(message)
      local sandbox = {}
      for _,elem in pairs({"coroutine","assert","tostring","tonumber","ipairs","pcall","pairs","bit","debug","string","unpack","table","collectgarbage","next","math","select","type"}) do
        sandbox[elem] = _G[elem]
      end
      sandbox.os = {clock=os.clock, time=os.time, date=os.date, difftime=os.difftime}
      local toReturn = {}
      sandbox.toReturn = toReturn
      sandbox.client = nil
      sandbox.message, sandbox.toReturn = message, toReturn
      sandbox.p = function( ... )
        local answer = {}
        for _, value in pairs({ ... }) do
          if type(value) == "table" then
            table.insert(answer, table.tostring(value))
          else
            table.insert(answer, tostring(value))
          end
        end
        table.insert(toReturn, table.concat(answer, "\n"))
      end
      sandbox.print = sandbox.p

      load(arg, "", "t", sandbox)()
      table.insert(toReturn, 1, "**SUCCESS**")
      bigMessage(toReturn, message, message.author:getMentionString(), "```")
    end, message)
    if not success then
      sendAndDelete(message.channel, message.author:getMentionString().."\n```\n"..err.."\n```", serverdata[message.server.id].deleteDelay)
    end
  end
}

commands["стихи"] = {
  help = "poems",
  langsWhitelist = {"ru"},

  split = false,
  group = "user",
  allowMultiple = true,

  func = function( message, arg )
    pathToPoems = "./assets/poems_"..serverdata[message.server.id].lang..".txt"
    commandsData.poems = commandsData.poems or {}
    if ( arg == "reload" and message.author.id == config.authorID ) or not commandsData.poems[serverdata[message.server.id].lang] then
      commandsData.poems[serverdata[message.server.id].lang] = {}
      local poemFile = io.open(pathToPoems)
      if poemFile then
        local poem = {}
        for pLine in poemFile:lines() do
          if pLine == "----" then
            if #poem > 0 then
              if string.utf8len(table.concat(poem, "\n")) <= 1900 then
                table.insert(commandsData.poems[serverdata[message.server.id].lang], table.concat(poem, "\n"))
              else
                writeLog("Too long poem!", "ERR")
              end
              poem = {}
            end
          else
            table.insert(poem, pLine)
          end
        end
        if #poem > 0 then
          table.insert(commandsData.poems[serverdata[message.server.id].lang], table.concat(poem, "\n"))
        end
        sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.poems.loaded, message.author:getMentionString(), #commandsData.poems[serverdata[message.server.id].lang]), serverdata[message.server.id].deleteDelay)
      else
        commandsData.poems[serverdata[message.server.id].lang] = nil
        writeLog("[Can't open "..tostring(pathToPoems).."!", "ERR")
        sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.poems.error, message.author:getMentionString()), serverdata[message.server.id].deleteDelay)
        return
      end
    end
    sendAndDelete(message.channel, message.author:getMentionString().."\n"..selectRandom(commandsData.poems[serverdata[message.server.id].lang]), serverdata[message.server.id].deleteDelay)
  end
}

commands["execute"] = {
  help = "execute",

  split = false,
  group = "owner",

  func = function( message, arg )
    if arg then
      if string.find(arg, "rm ") then sendAndDelete(message.channel, "```Execution Error```", serverdata[message.server.id].deleteDelay);return end
      local answer = {}
      os.execute(arg.." > ./assets/temp.txt")
      if not io.open("temp.txt") then
        for TTline in io.lines("./assets/temp.txt") do
          table.insert(answer, TTline)
        end
        bigMessage(answer, message, message.author:getMentionString())
      else
        sendAndDelete(message.channel, "```Execution Error```", serverdata[message.server.id].deleteDelay)
      end
    end
  end
}

commands["uptime"] = {
  help = "uptime",

  split = false,
  group = "admin",
  
  func = function(message, arg)
    sendAndDelete(message.channel, message.author:getMentionString()..", "..secToTime(os.time()-startTime), serverdata[message.server.id].deleteDelay)
  end
}

commands["победители"] = {
  help = "winners",
  langsWhitelist = {"ru"},

  split = false,
  group = "user",
  whitelist = {"tlauncher"},
  disabled = true,

  func = function( message, arg )
    commandsData.winners = commandsData.winners or {}
    commandsData.winners.time = commandsData.winners.time or os.time()
    if os.time()-commandsData.winners.time >= 600 or commandsData.winners.data == nil then
      -- перезагружаем CSV
      local winnersFile = csv.open(makePath("./assets/winners.csv"))
      if not winnersFile then
        sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.winners.nofile, message.author:getMentionString()), serverdata[message.server.id].deleteDelay)
        writeLog("No assets/winners.csv!", "WARN")
        return
      end
      commandsData.winners.data = {}
      for fields in winnersFile:lines() do
        if (fields[2] == "" or fields[2] == nil) and (fields[4] ~= "" and fields[4] ~= nil) then
          local toInsert = (fields[3] == nil or fields[3] == "") and fields[4] or fields[3].." ("..fields[4]..")"
          toInsert = ":heavy_check_mark: "..toInsert
          table.insert(commandsData.winners.data, toInsert)
        elseif fields[2] ~= "" and fields[2] ~= nil then
          local toInsert = (fields[1] == nil or fields[1] == "") and fields[2] or fields[1].." ("..fields[2]..")"
          table.insert(commandsData.winners.data, toInsert)
        end
      end
      winnersFile:close()
    end

    -- выводим CSV
    bigMessage(commandsData.winners.data, message, string.format(texts[serverdata[message.server.id].lang].commands.winners.winners, message.author:getMentionString()))
  end
}

commands["temper"] = {
  help = "temper",

  split = false,
  group = "user",

  func = function( message, arg )
    texts[serverdata[message.server.id].lang].commands.temper.tempers = texts[serverdata[message.server.id].lang].commands.temper.tempers or {}
    sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.temper.message, message.author:getMentionString(), texts[serverdata[message.server.id].lang].commands.temper.tempers[commandsData.temper.temper] or commandsData.temper.temper, commandsData.temper.count), serverdata[message.server.id].deleteDelay)
  end
}

commands["stats"] = {
  help = "stats",

  split = false,
  group = "user",

  func = function( message, args )
    -- temp
    local function getlangs()
      local answer
      for langName in pairs(texts) do
        answer = answer and answer..", "..langName or langName
      end
      return answer
    end
    -- get all data
    local users, servers = 0, 0
    for _, Tserver in pairs(client.servers) do
      users = users + table.size(Tserver.members, true)
      servers = servers + 1
    end
    local langsList = getlangs()
    local langsCount = table.size(texts, true)
    answer = string.format([[```
Name: %s
Original name: Answernator T-800
Version: %s
Language: Lua
Framevork: Luvit
Library: Discordia
Langs support: %s (total %s)

Uptime: %s
Servers: %s
Users: %s
```]], client.user.username, botVersion or "NUL", langsList, langsCount, secToTime(os.time()-startTime), servers, users)
    sendAndDelete(message.channel, answer, serverdata[message.server.id].deleteDelay)
  end
}

commands["configure"] = {
  help = "configure",

  split = 3,
  group = "admin",
  beta = true,

  func = function( message, args )
    if args[2] then args[2] = string.utf8lower(args[2]) end
    if args[2] == "help" then
      bigMessage(texts[serverdata[message.server.id].lang].commands.configure.help, message, message.author:getMentionString())
    elseif args[2] == "lang" then
      if texts[args[3]] then
        serverdata[message.server.id].lang = args[3]
        sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.configure.success, message.author:getMentionString(), args[2], args[3]), serverdata[message.server.id].deleteDelay)
      else
        sendAndDelete(message.channel, texts[serverdata[message.server.id].lang].commands.configure.error, serverdata[message.server.id].deleteDelay)
      end
    elseif args[2] == "deletemessages" or args[2] == "usenickblacklist" or args[2] == "greetnewusers" or args[2] == "usebottalking" or args[2] == "deletebotmessages" then
      args[2] = args[2]=="deletemessages" and "deleteMessages" or args[2]=="usenickblacklist" and "useNickBlacklist" or args[2]=="greetnewusers" and "greetNewUsers" or args[2]=="usebottalking" and "useBotTalking" or args[2]=="deletebotmessages" and "deleteBotMessages"
      if args[3] == "true" or args[3] == "enable" then
        serverdata[message.server.id][args[2]] = true
        sendAndDelete(message.channel, texts[serverdata[message.server.id].lang].commands.configure.error, serverdata[message.server.id].deleteDelay)
      elseif args[3] == "false" or args[3] == "disable" then
        serverdata[message.server.id][args[2]] = false
        sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.configure.success, message.author:getMentionString(), args[2], args[3]), serverdata[message.server.id].deleteDelay)
      else
        sendAndDelete(message.channel, texts[serverdata[message.server.id].lang].commands.configure.error, serverdata[message.server.id].deleteDelay)
      end
    elseif args[2] == "deletedelay" then
      if tonumber(args[3]) then
        serverdata[message.server.id].deleteDelay = tonumber(args[3])
        sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.configure.success, message.author:getMentionString(), args[2], args[3]), serverdata[message.server.id].deleteDelay)
      else
        sendAndDelete(message.channel, texts[serverdata[message.server.id].lang].commands.configure.error, serverdata[message.server.id].deleteDelay)
      end
    elseif args[2] == "greeting" then
      if args[3] then
        serverdata[message.server.id].greeting = args[3]
        sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.configure.textSet, message.author:getMentionString()), serverdata[message.server.id].deleteDelay)
      else
        sendAndDelete(message.channel, texts[serverdata[message.server.id].lang].commands.configure.error, serverdata[message.server.id].deleteDelay)
      end
    elseif args[2] == "defaultchannel" then
      if table.size(message.mentions.channels, true) == 1 then for key in pairs(message.mentions.channels) do args[3] = key;break end end
      if message.server:getChannelById(args[3]) then
        serverdata[message.server.id].defaultChannel = args[3]
        sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.configure.success, message.author:getMentionString(), args[2], message.server:getChannelById(args[3]).name.."(ID "..message.server:getChannelById(args[3]).id..")"), serverdata[message.server.id].deleteDelay)
      else
        sendAndDelete(message.channel, texts[serverdata[message.server.id].lang].commands.configure.error, serverdata[message.server.id].deleteDelay)
      end
    elseif args[2] == "addnick" then
      if args[3] then
        args[3] = string.utf8lower(args[3])
        if tableFind(serverdata[message.server.id].nickBlacklist, args[3])==false then
          table.insert(serverdata[message.server.id].nickBlacklist, args[3])
          sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.configure.wordAdded, message.author:getMentionString(), args[3]), serverdata[message.server.id].deleteDelay)
        else
          sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.configure.wordAlreadyAdded, message.author:getMentionString(), args[3]), serverdata[message.server.id].deleteDelay)
        end
        sendAndDelete(message.channel, texts[serverdata[message.server.id].lang].commands.configure.error, serverdata[message.server.id].deleteDelay)
      end
    elseif args[2] == "removenick" then
      if args[3] then
        args[3] = string.utf8lower(args[3])
        local found = false
        for iter,value in ipairs(serverdata[message.server.id].nickBlacklist) do
          if value==args[3] then
            found=true
            table.remove(serverdata[message.server.id].nickBlacklist, iter)
            break
          end
        end
        if found == true then
          sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.configure.wordDeleted, message.author:getMentionString(), args[3]), serverdata[message.server.id].deleteDelay)
        else
          sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.configure.wordNotFound, message.author:getMentionString(), args[3]), serverdata[message.server.id].deleteDelay)
        end
      else
        sendAndDelete(message.channel, texts[serverdata[message.server.id].lang].commands.configure.error, serverdata[message.server.id].deleteDelay)
      end
    elseif args[2] == "addrole" then
      if args[3] then
        if not serverdata[message.server.id].whitelistedRoles[args[3]] then
          serverdata[message.server.id].whitelistedRoles[args[3]]=true
          sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.configure.roleAdded, message.author:getMentionString(), message.server:getRoleById(args[3]).name), serverdata[message.server.id].deleteDelay)
        else
          sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.configure.roleNotFound, message.author:getMentionString(), args[3]), serverdata[message.server.id].deleteDelay)
        end
      else
        sendAndDelete(message.channel, texts[serverdata[message.server.id].lang].commands.configure.error, serverdata[message.server.id].deleteDelay)
      end
    elseif args[2] == "removerole" then
      if args[3] then
        if serverdata[message.server.id].whitelistedRoles[args[3]] then
          serverdata[message.server.id].whitelistedRoles[args[3]]=nil
          sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.configure.roleDeleted, message.author:getMentionString(), message.server:getRoleById(args[3]) and message.server:getRoleById(args[3]).name or args[3]), serverdata[message.server.id].deleteDelay)
        else
          sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.configure.roleNotFound, message.author:getMentionString(), args[3]), serverdata[message.server.id].deleteDelay)
        end
      else
        sendAndDelete(message.channel, texts[serverdata[message.server.id].lang].commands.configure.error, serverdata[message.server.id].deleteDelay)
      end
    elseif args[2] == "kicklimit" or args[2] == "banlimit" then
      if tonumber(args[3]) then
        serverdata[message.server.id].votesLimits[args[2]=="kicklimit" and "kick" or "ban"] = tonumber(args[3])
        sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.configure.success, message.author:getMentionString(), args[2], args[3]), serverdata[message.server.id].deleteDelay)
      else
        sendAndDelete(message.channel, texts[serverdata[message.server.id].lang].commands.configure.error, serverdata[message.server.id].deleteDelay)
      end
    elseif args[2]=="vk" or args[2] == "fb" or args[2] == "links" then
      if args[3] then
        local file = io.open(makePath("serverdata/"..message.server.id.."/"..args[2]..".txt"), "w")
        file:write(args[3])
        file:close()
        sendAndDelete(message.channel, string.format(texts[serverdata[message.server.id].lang].commands.configure.textSet, message.author:getMentionString()), serverdata[message.server.id].deleteDelay)
      else
        sendAndDelete(message.channel, texts[serverdata[message.server.id].lang].commands.configure.error, serverdata[message.server.id].deleteDelay)
      end
    else
      local answer = {}
      for _, value in pairs({"lang", "deleteMessages", "deleteBotMessages", "deleteDelay", "useNickBlacklist", "greetNewUsers", "greeting", "useBotTalking", "defaultChannel"}) do
        table.insert(answer, texts[serverdata[message.server.id].lang].commands.configure[value])
        table.insert(answer, tostring(serverdata[message.server.id][value]))
      end
      table.insert(answer, texts[serverdata[message.server.id].lang].commands.configure.nickBlacklist)
      table.insert(answer, table.concat(serverdata[message.server.id].nickBlacklist, ", "))
      table.insert(answer, texts[serverdata[message.server.id].lang].commands.configure.whitelistedRoles)
      local answer2 = {}
      for roleid in pairs(serverdata[message.server.id].whitelistedRoles) do
        local role = message.server:getRoleById(roleid)
        table.insert(answer2, (role and role.name or "**Unknown**").."(ID "..roleid..")")
      end
      table.insert(answer, table.concat(answer2 or {}, ", "))
      table.insert(answer, string.format(texts[serverdata[message.server.id].lang].commands.configure.votesLimits, serverdata[message.server.id].votesLimits.kick or "NIL", serverdata[message.server.id].votesLimits.ban or "NIL"))
      table.insert(answer, texts[serverdata[message.server.id].lang].commands.configure.usehelp)
      bigMessage(answer, message, message.author:getMentionString())
    end
    local file = io.open(makePath("serverdata/"..message.server.id.."/server.json"), "w")
    file:write(json.stringify(serverdata[message.server.id]))
    file:close()
  end
}

commands["broadcast"] = {
  help = "broadcast",

  split = false,
  group = "owner",

  func = function(message, args)
    if args then
      for _,server in pairs(client.servers) do
        server:getChannelById(serverdata[server.id].defaultChannel):sendMessage(args)
      end
    else
      sendAndDelete(message.channel, texts[serverdata[message.server.id].lang].commands.broadcastError, serverdata[message.server.id].deleteDelay)
    end
  end
}

commands["mute"] = {
  help = "mute",

  split = false,
  group = "permission",
  perm = "manageMessages",
  beta = true,

  func = function( message, args )
    message.channel:sendMessage("**WARNING!**\nMute - is in early beta!")
    -- replace with message.server.defaultRole.permissions.__class after 1.0
    local allowPerm, denyPerm = message.server.defaultRole.permissions.__index(), message.server.defaultRole.permissions.__index()
    denyPerm:set("sendMessages")
    for _, member in pairs(message.mentions.members) do
      if checkWhitelist(member, message.server) == false then
        message.channel:editPermissionsFor(member, allowPerm, denyPerm)
        print("Member "..member.name.."(ID "..member.id..") muted")
      else
        print("Can't mute member "..member.name.."(ID "..member.id..")")
      end
    end
  end
}

commands["muteall"] = {
  help = "unmute",

  split = false,
  group = "permission",
  perm = "manageMessages",
  beta = true,

  func = function( message, args )
    message.channel:sendMessage("**WARNING!**\nMute - is in early beta!")
    -- replace with message.server.defaultRole.permissions.__class after 1.0
    local allowPerm, denyPerm = message.server.defaultRole.permissions.__index(), message.server.defaultRole.permissions.__index()
    denyPerm:set("sendMessages")
    for _, member in pairs(message.mentions.members) do
      if checkWhitelist(member, message.server) == false then
        for _,channel in pairs(message.server.channels) do
          channel:editPermissionsFor(member, allowPerm, denyPerm)
        end
        print("Member "..member.name.."(ID "..member.id..") muted")
      else
        print("Can't mute member "..member.name.."(ID "..member.id..")")
      end
    end
  end
}

commands["unmute"] = {
  help = "unmute",

  split = false,
  group = "permission",
  perm = "manageMessages",
  beta = true,

  func = function( message, args )
    message.channel:sendMessage("**WARNING!**\nMute - is in early beta!")
    -- replace with message.server.defaultRole.permissions.__class after 1.0
    local allowPerm, denyPerm = message.server.defaultRole.permissions.__index(), message.server.defaultRole.permissions.__index()
    for _, member in pairs(message.mentions.members) do
      if checkWhitelist(member, message.server) == false then
        for _,channel in pairs(message.server.channels) do
          channel:editPermissionsFor(member, allowPerm, denyPerm)
        end
        print("Member "..member.name.."(ID "..member.id..") unmuted")
      else
        print("Can't unmute member "..member.name.."(ID "..member.id..")")
      end
    end
  end
}

commands["phrases"] = {
  help = commands["фразы"].help,

  split = commands["фразы"].split,
  group = commands["фразы"].group,
  disabled = commands["фразы"].disabled,

  func = commands["фразы"].func
}

commands["status"] = {
  help = commands["статус"].help,

  split = commands["статус"].split,
  group = commands["статус"].group,
  disabled = commands["статус"].disabled,

  func = commands["статус"].func
}

commands["vk"] = {
  help = commands["вк"].help,

  group = commands["вк"].group,
  split = commands["вк"].split,

  func = commands["вк"].func
}

commands["links"] = {
  help = commands["ссылки"].help,

  group = commands["ссылки"].group,
  split = commands["ссылки"].split,

  func = commands["ссылки"].func
}

commands["список"] = {
  help = commands["help"].help,
  langsWhitelist = {"ru"},

  group = commands["help"].group,
  split = commands["help"].split,

  func = commands["help"].func
}

commands["info"] = commands["help"]
commands["cmds"] = commands["help"]

----------------------------
--- Добавляем стринги помощи
----------------------------
p(os.date("[%d.%m.%y][%X]").."[INFO][CMDS] Started help strings parser")
for commandName, commandTable in pairs(commands) do
  if commandTable.langsWhitelist then
    for _, langName in pairs(commandTable.langsWhitelist) do
      if texts[langName] then commands[commandName][langName] = texts[langName].help[commandTable.help or commandName] end
      print("'"..os.date("[%d.%m.%y][%X]").."[INFO][CMDS] Added help string "..(commandTable.help or commandName).." for command "..commandName..", lang: "..langName.."'")
    end
  else
    for langName in pairs(texts) do
      commands[commandName][langName] = texts[langName].help[commandTable.help or commandName]
      print("'"..os.date("[%d.%m.%y][%X]").."[INFO][CMDS] Added help string "..(commandTable.help or commandName).." for command "..commandName..", lang: "..langName.."'")
    end
  end
end
p(os.date("[%d.%m.%y][%X]").."[INFO][CMDS] All help strings parsed")
for commandName in pairs(commands) do
  commands[commandName].help, commands[commandName].langsWhitelist = nil, nil
end
p(os.date("[%d.%m.%y][%X]").."[INFO][CMDS] All trash removed")

----------------------------
---  Выводим инфу в консоль
----------------------------

for cmdName, cmd in pairs(commands) do
  print("'"..os.date("[%d.%m.%y][%X]").."[INFO][CMDS] Loaded "..((cmd.disabled and "disabled") or cmd.group or "user").." command "..cmdName.."'")
  if config.removeDisabledCommands and cmd.disabled then commands[cmdName] = nil end
end
p(os.date("[%d.%m.%y][%X]").."[INFO][CMDS] All commands loaded")

return commands