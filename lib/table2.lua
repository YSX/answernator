-- table extension library by ECS

local function doSerialize(array, text, prettyLook, indentationSymbol, oldIndentationSymbol, equalsSymbol, currentRecusrionStack, recursionStackLimit)
  text = {"{"}
  table.insert(text, (prettyLook and "\n" or nil))
  
  for key, value in pairs(array) do
    local keyType, valueType, stringValue = type(key), type(value), tostring(value)

    if keyType == "number" or keyType == "string" then
      table.insert(text, (prettyLook and indentationSymbol or nil))
      table.insert(text, "[")
      table.insert(text, (keyType == "string" and table.concat({"\"", key, "\""}) or key))
      table.insert(text, "]")
      table.insert(text, equalsSymbol)
      
      if valueType == "number" or valueType == "boolean" or valueType == "nil" then
        table.insert(text, stringValue)
      elseif valueType == "string" or valueType == "function" then
        table.insert(text, "\"")
        table.insert(text, stringValue)
        table.insert(text, "\"")
      elseif valueType == "table" then
        -- Ограничение стека рекурсии
        if currentRecusrionStack < recursionStackLimit then
          table.insert(text, table.concat(doSerialize(value, text, prettyLook, table.concat({indentationSymbol, indentationSymbol}), table.concat({oldIndentationSymbol, indentationSymbol}), equalsSymbol, currentRecusrionStack + 1, recursionStackLimit)))
        else
          table.insert(text, "...")
        end
      else
        -- error("Unsupported table value type: " .. valueType)
      end
      
      table.insert(text, ",")
      table.insert(text, (prettyLook and "\n" or nil))
    else
      -- error("Unsupported table key type: " .. keyType)
    end
  end

  table.remove(text, (prettyLook and #text - 1 or #text))
  table.insert(text, (prettyLook and oldIndentationSymbol or nil))
  table.insert(text, "}")
  return text
end

function table.serialize(array, prettyLook, indentationWidth, indentUsingTabs, recursionStackLimit)
  indentationWidth = indentationWidth or 2
  local indentationSymbol = indentUsingTabs and " " or " "
  indentationSymbol, indentationSymbolHalf = string.rep(indentationSymbol, indentationWidth)
  return table.concat(doSerialize(array, {}, prettyLook, indentationSymbol, "", prettyLook and " = " or "=", 1, recursionStackLimit or math.huge))
end

function table.unserialize(serializedString)
  local success, result = pcall(load("return " .. serializedString))
  if success then return result else return nil, result end
end

function table.toString(...)
  return table.serialize(...)
end

function table.tostring(...)
  return table.serialize(...)
end

function table.fromString(...)
  return table.unserialize(...)
end

function table.toFile(path, array, prettyLook, indentationWidth, indentUsingTabs, recursionStackLimit, appendToFile)
  local file = io.open(path, appendToFile and "a" or "w")
  file:write(table.serialize(array or {}, prettyLook, indentationWidth, indentUsingTabs, recursionStackLimit))
  file:close()
end

function table.fromFile(path)
  local file = io.open(path, "r")
  if not file then return {} end
  local data = table.unserialize(file:read("*a"))
  file:close()
  return data
end

function table.copy(tableToCopy)
  local function recursiveCopy(source, destination)
    for key, value in pairs(source) do
      if type(value) == "table" then
        destination[key] = {}
        recursiveCopy(source[key], destination[key])
      else
        destination[key] = value
      end
    end
  end

  local tableThatCopied = {}
  recursiveCopy(tableToCopy, tableThatCopied)

  return tableThatCopied
end

function table.binarySearch(t, requestedValue)
  local function recursiveSearch(startIndex, endIndex)
    local difference = endIndex - startIndex
    local centerIndex = math.floor(difference / 2 + startIndex)

    if difference > 1 then
      if requestedValue >= t[centerIndex] then
        return recursiveSearch(centerIndex, endIndex)
      else
        return recursiveSearch(startIndex, centerIndex)
      end
    else
      if math.abs(requestedValue - t[startIndex]) > math.abs(t[endIndex] - requestedValue) then
        return t[endIndex]
      else
        return t[startIndex]
      end
    end
  end

  return recursiveSearch(1, #t)
end

function table.size(t, ttt)
  local size = #t
  if size == 0 or ttt == true then size=0;for key in pairs(t) do size = size + 1 end end
  return size
end