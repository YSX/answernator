-- To add this bot on your channel
-- https://discordapp.com/oauth2/authorize?client_id=227546903145873408&scope=bot

local discordia = require('discordia')
_G.colorize = require('pretty-print').colorize
_G.timer = require("timer")
_G.csv = require("lib/csv")
_G.fs = require('fs')
_G.json = require('json')
local path = require('path')
require("lib/utf8data")
require("lib/utf8")
require("lib/table2")
_G.client = discordia.Client:new()
_G.startTime = os.time()

_G.makePath = function( somePath )
  local newstr = somePath:gsub("/", path.getSep())
  return newstr
end

_G.writeLog = function( text, logType )
  logType = string.upper(logType or "INFO")
  local logColors = { INFO = "string", LOG = "string", WARN = "highlight", DEBUG= "highlight", ERR = "err", ERROR= "err", FAIL = "failure", FAILURE = "failure"}
  print(colorize(logColors[logType] or "string", "'"..os.date("[%d.%m.%y][%X]").."["..logType.."] "..text.."'"))
  local logfile = io.open(makePath("log/bot.log"), "a")
  if logfile then
    logfile:write(os.date("[%d.%m.%y][%X]").."["..logType.."] "..text.."\n")
    logfile:close()
  else
    print(colorize('failure', "'"..os.date("[%d.%m.%y][%X]").."[ERR] Can't open log file!'"))
  end
end

-- config load
if fs.existsSync("config.json") then
  _G.config = json.parse(fs.readFileSync("config.json"))
else
  _G.config = {}
end
local defaultConfig = {
  authorID = "135017849604276224",
  token = "insert-token-here",
  commandSymbol = "!",
  adminRole = "answernatoraccess",
  whitelistedRole = "answernatorwhitelist",
  removeDisabledCommands = false,
  defaultLang = "ru",
  messageDeleteDelay = 60000
}
for key in pairs(defaultConfig) do
  _G.config[key] = config[key] or defaultConfig[key]
  writeLog("Loaded config key "..key)
end
local file = io.open("config.json", "w")
file:write(json.stringify(config))
file:close()
-- config load ends

---- functions
_G.sendAndDelete = function( channel, message, Ttimer )
  Ttimer = Ttimer or 3000
  local Tmessage = channel:sendMessage(message)
  if Tmessage and serverdata[channel.server.id].deleteBotMessages==true then
    timer.setTimeout(Ttimer, coroutine.wrap(Tmessage.delete), Tmessage)
  end
end

_G.checkPerm = function( TMmember, TMserver )
  if TMmember.id == config.authorID then return true end
  if TMmember.id == TMserver.owner.id then return true end
  for _, role in pairs(TMmember.roles) do
    if string.utf8lower(role.name) == config.adminRole then return true end
  end
  return false
end

_G.noAccessMessage = function( TTmessage )
  local Targs = {message = TTmessage.content, authorID = TTmessage.author.id, authorName = TTmessage.author.name, server = TTmessage.server.name, serverID = TTmessage.server.id, channel = TTmessage.channel.name, channelID = TTmessage.channel.id}
  writeLog("No acces to command, arguments: "..table.tostring(Targs), "WARN")
  sendAndDelete(TTmessage.channel, string.format(texts[serverdata[TTmessage.server.id].lang].admin.noAccess, TTmessage.author:getMentionString()), 3000)
  TTmessage:delete()
end

_G.selectRandom = function( massiv )
  return massiv[math.random(#massiv)]
end

function find( text, keywords )
  if #keywords == 0 then return false end
  local count = 0
  text = string.utf8lower(text)
  for _,v in pairs(keywords) do
    if type(v) == type({}) then
      for _,v2 in pairs(v) do
        if string.find(text, v2) then count = count+1;break end
      end
    else
      if string.find(text, v) then count = count+1 end
    end
  end
  if count == #keywords then return true else return false end
end

_G.loadTable = function( file )
  TTdata = table.fromFile(file)
  if type(TTdata) == "table" then return TTdata else return {} end
end

_G.hasPermission = function( someMember, somePermission )
  for _, role in pairs(someMember.roles) do
    if role.permissions:hasPermission(somePermission) then return true end
  end
  return false
end

function findSome( text1, keywords1 )
  for _,v in pairs(keywords1) do
    if find(text1, v) then return true end
  end
  return false
end

_G.save = function( toSave, name )
  local dump = io.open("dumps/"..name..".dump", "w")
  dump:write(table.tostring(toSave, true, 2, false, 10))
  dump:close()
  writeLog("Saved "..name)
end

_G.createEmptyFile = function( somePath )
  io.open(somePath, "w"):close()
end

_G.tableFind = function( Ttable, Tquerry )
  for _,xx in pairs(Ttable) do
    if xx == Tquerry then return true end
  end
  return false
end

_G.getGameName = function( nameFile, wordsCount )
  wordsCount = wordsCount == "random" and math.random(2, 3) or wordsCount or 3
  if not word_list then
    writeLog("No game names loaded, starting load...")
    if not io.open(nameFile) then
      writeLog("Can't open "..nameFile, "ERR")
      return "ERROR"
    end
    _G.word_list = {}
    local Twords = {}
    for Tstring in io.open(nameFile):lines() do
      if Tstring == "----" then table.insert(word_list, Twords);Twords = {} else table.insert(Twords, Tstring) end
    end
    if #Twords ~= 0 then table.insert(word_list, Twords) end
  end
  local badMatchList, words = {}, {}
  local Tnum = wordsCount == 3 and 1 or math.random(1, 2)
  repeat
    local word = word_list[#words+Tnum][math.random(#word_list[#words+Tnum])]
    if string.find(word, "^") then
      local Tword, badWords = string.match(word, "([^%^]+)(.*)")
      word = Tword or word
      if badWords then
        for badWord in string.gmatch(badWords ,"[^%|]+") do
          table.insert(badMatchList, badWord)
        end
      end
    end

    if not tableFind(badMatchList, word) and not tableFind(words, word) then table.insert(words, word) end
  until #words == wordsCount
  return table.concat(words, " ")
end


--[[BEGIN]]
--- check all needed dirs
local dirsList = {"dumps", "log", "assets", makePath("assets/lang"), "serverdata"}
for _, dir in pairs(dirsList) do
  if not fs.existsSync(dir) then
    fs.mkdirSync(dir)
    writeLog("Created missing folder "..dir)
  end
end
dirsList = nil
-- dirs checked

-- load all available langs
_G.texts = {}
for langFile in fs.scandirSync(makePath("assets/lang")) do
  if string.find(langFile, "^[%w]+%.lang$") then
    writeLog("Found new lang: "..langFile)
    texts[string.match(langFile, "([%w]+)%.lang")] = dofile("assets/lang/"..langFile)
    writeLog("Loaded new lang: "..langFile)
  end
end
-- langs load end
-- load server data
_G.serverdata, _G.votedata = {}, {}
for server in fs.scandirSync("serverdata") do
  if server:find("^[%d]+$") and fs.existsSync(makePath("serverdata/"..server.."/server.json")) then
    serverdata[server] = json.parse(fs.readFileSync(makePath("serverdata/"..server.."/server.json")))
  end
  if server:find("^[%d]+$") and fs.existsSync(makePath("serverdata/"..server.."/votes.json")) then
    votedata[server] = json.parse(fs.readFileSync(makePath("serverdata/"..server.."/votes.json")))
  end
end
-- end
_G.commandsData = {timeouts={}}
local previousCommand = loadTable("dumps/previousCommand.dump")
local commands = dofile("commands.lua")
math.randomseed(os.time()) -- пусть рандом будет рандомнее

-- load bot temper
commandsData.temper = loadTable("./dumps/temper.dump")
commandsData.temper.temper = commandsData.temper.temper or "neutral"
commandsData.temper.count = commandsData.temper.count or 0
writeLog("Set temper to "..commandsData.temper.temper..", temper count: "..commandsData.temper.count)


client:on('ready', function()
  writeLog("Logged in as "..client.user.username, "INFO")
end)

client:on('memberJoin', function(member)
  if serverdata[member.server.id].useNickBlacklist == true then
    for _, blacklistedWord in pairs(serverdata[member.server.id].nickBlacklist) do
      if string.find(string.utf8lower(member.name), blacklistedWord) then
        member.server:getChannelById(serverdata[member.server.id].defaultChannel):sendMessage(string.format(texts[serverdata[member.server.id].lang].events["newBadMember"], member:getMentionString()))
        member.server:banUser(member)
        writeLog("Member changed nick and banned: " .. member.username..", id "..member.id..", server: "..member.server.name, "WARN")
        return
      end
    end
  end
  if serverdata[member.server.id].greetNewUsers == true then
    member.server:getChannelById(serverdata[member.server.id].defaultChannel):sendMessage(string.format(serverdata[member.server.id].greeting, member:getMentionString()))
  end
  writeLog("New member joined: " .. member.username..", id "..member.id..", server: "..member.server.name.." (ID: "..member.server.id..")")
end)

client:on('resume', function()
  writeLog("Resumed as "..client.user.username, "INFO")
end)

client:on('disconnect', function(expected)
  writeLog(expected == true and "Expected disconnect" or "UNEXPECTED DISCONNECT!", "WARN")
  save(previousCommand, "previousCommand")
  save(commandsData.temper, "temper")
  writeLog("All dumps saved", "INFO")
end)


client:on('memberUpdate', function(memberUpd)
  if serverdata[memberUpd.server.id].useNickBlacklist == true then
    for _, blacklistedWord in pairs(serverdata[memberUpd.server.id].nickBlacklist) do
      if string.find(string.utf8lower(memberUpd.name), blacklistedWord) then
        memberUpd.server:getChannelById(serverdata[memberUpd.server.id].defaultChannel):sendMessage(string.format(texts[serverdata[memberUpd.server.id].lang].events["newBadMember"], memberUpd:getMentionString()))
        memberUpd.server:banUser(memberUpd)
        writeLog("[WARN] Member changed nick and banned: " .. memberUpd.username..", id "..memberUpd.id..", server: "..memberUpd.server.name, "WARN")
        return
      end
    end
  end
end)

client:on('serverCreate', function(newServer)
  local serverIsNew
  if not fs.existsSync(makePath("serverdata/"..newServer.id)) then
    fs.mkdirSync(makePath("serverdata/"..newServer.id))
    serverIsNew=true
  end
  -- defaults
  local defaultServerConfig = {
    lang = config.defaultLang,
    deleteMessages = true,
    deleteDelay = config.messageDeleteDelay,
    useNickBlacklist = false,
    nickBlacklist = {},
    greetNewUsers = true,
    greeting = texts[config.defaultLang].events.newMember,
    useBotTalking = true,
    whitelistedRoles = {},
    votesLimits = {},
    defaultChannel = newServer.defaultChannel.id,
    deleteBotMessages = true
  }
  serverdata[newServer.id] = serverdata[newServer.id] or {}
  for key, value in pairs(defaultServerConfig) do
    serverdata[newServer.id][key] = serverdata[newServer.id][key] or value
  end
  local file = io.open(makePath("serverdata/"..newServer.id.."/server.json"), "w")
  file:write(json.stringify(serverdata[newServer.id]))
  file:close()

  if serverIsNew then
    -- check permissions
    local neededPerms =  {"kickMembers", "banMembers", "readMessages", "sendMessages", "manageMessages", "createInstantInvite"}
    for _, role in pairs(newServer.me.roles) do
      for flagName, flag in pairs(neededPerms) do
        if role.permissions:hasPermission(flag) then neededPerms[flagName]=nil end
      end
    end
    local missingPerms
    if table.size(neededPerms, true) > 0 then
      missingPerms = "```"..table.concat(neededPerms, ", ").."```"
    end
    -- --
    newServer:getChannelById(serverdata[newServer.id].defaultChannel):sendMessage(string.format(texts[serverdata[newServer.id].lang].events.newserver.base, missingPerms or texts[serverdata[newServer.id].lang].events.newserver.allperms))
    writeLog("New server discovered. ID: "..newServer.id..", name: "..newServer.name)
    client:getMemberById(config.authorID):sendMessage("New server discovered.\nID: "..newServer.id.."\nName: "..newServer.name)
  end
end)

client:on('channelDelete', function( channel )
  if not channel.server then return end
  if channel.id == serverdata[channel.server.id].defaultChannel then
    writeLog("Default bot channel "..channel.name.." was deleted on server "..server.name..", switching to default")
    serverdata[channel.server.id].defaultChannel = channel.server.defaultChannel.id
    channel.server.defaultChannel:sendMessage(texts[serverdata[channel.server.id].lang].events.removedChannel)
    channel.server.owner:sendMessage(texts[serverdata[channel.server.id].lang].events.removedChannel.."\nServer: "..channel.server.name)
  end
end)


-- Это для общительности
client:on('messageCreate', function(message)
  if message.content == nil or message.content == "" or message.author == client.user or message.server == nil then return end

  if serverdata[message.server.id].useBotTalking == false then return end

  for phraseName, phrase in pairs(texts[serverdata[message.server.id].lang].phrases) do
    if phrase.triggers and (phrase.answers or phrase.temperAnswers[commandsData.temper.temper]) then
      if findSome(message.content, phrase.triggers) then
        if phrase.exceptions then
          for _, someString in pairs(phrase.exceptions) do
            if type(someString) == type({}) then
              local count = 0
              for _, someOtherString in pairs(someString) do
                if string.find(message.content, someOtherString) then
                  count = count + 1
                end
              end
              if count == #someString then return end
            else
              if string.find(message.content, someString) then
                return
              end
            end
          end
        end
        writeLog(string.format("User %s (ID: %s) triggered phrase %s on server %s (ID: %s)", message.author.name, message.author.id, phraseName, message.server.name, message.server.id), "INFO")
        if phrase.temperAnswers then
          if phrase.temperAnswers[commandsData.temper.temper] then
            message.channel:sendMessage(string.format(selectRandom(phrase.temperAnswers[commandsData.temper.temper]), message.author:getMentionString()))
          else
            message.channel:sendMessage(string.format(selectRandom(phrase.answers), message.author:getMentionString()))
          end
        else
          message.channel:sendMessage(string.format(selectRandom(phrase.answers), message.author:getMentionString()))
        end
        if phrase.temperBonus then
          commandsData.temper.count = commandsData.temper.count + phrase.temperBonus
          if commandsData.temper.count >= 10 then
            commandsData.temper.temper = "happy"
          elseif commandsData.temper.count >= 0 then
            commandsData.temper.temper = "neutral"
          elseif commandsData.temper.count >= -10 then
            commandsData.temper.temper = "unhappy"
          else
            commandsData.temper.temper = "angry"
          end
          writeLog("Temper set to "..commandsData.temper.temper..", temper count: "..commandsData.temper.count)
        end
        break
      end
    end
  end

end)


-- USER CMDs
client:on('messageCreate', function(cmd_message)
  -- exit early if the author is the same as the client
  if cmd_message.content == nil or cmd_message.content == "" or cmd_message.author == client.user or cmd_message.server == nil then return end

  if string.utf8sub(cmd_message.content, 1, string.utf8len(config.commandSymbol)) ~= config.commandSymbol then return end

  previousCommand[cmd_message.server.id] = previousCommand[cmd_message.server.id] or {}
  if not checkPerm(cmd_message.author, cmd_message.server) then
    if previousCommand[cmd_message.server.id][cmd_message.author.id] == cmd_message.content then
      writeLog("Trying to run previous command: "..cmd_message.content.." on server "..cmd_message.server.name..", ID: "..cmd_message.server.id..", sender: "..cmd_message.author.name)
      return
    end
  end

  local cmd, arguments = string.match(cmd_message.content, '(%S+) (.*)')
  cmd = cmd or cmd_message.content
  cmd = string.utf8lower(string.utf8sub(cmd, 1+string.utf8len(config.commandSymbol)))

  if cmd == "reload" then
    if cmd_message.author.id == config.authorID then
      save(commandsData.temper, "temper")
      save(previousCommand, "previousCommand")
      writeLog("All dumps saved. Starting text reload...")
      sendAndDelete(cmd_message.channel, os.date("[%d.%m.%y][%X]").."Texts reloading started", 3000)
      texts = {}
      for _, langFile in pairs(fs.readdirSync("assets/lang")) do
        if langFile:find("^[%w]+%.lang$") then
          writeLog("Found new lang: "..langFile)
          texts[string.match(langFile, "([%w]+)%.lang")] = dofile("assets/lang/"..langFile)
          writeLog("Loaded new lang: "..langFile)
        end
      end
      writeLog("Texts successfully reloaded. Starting commands reloading...")
      sendAndDelete(cmd_message.channel, os.date("[%d.%m.%y][%X]").."Texts reloaded. Commands reload started", 3000)
      commands={};commands=dofile("commands.lua", texts)
      sendAndDelete(cmd_message.channel, os.date("[%d.%m.%y][%X]").."Commands reloaded", 3000)
      writeLog("Resources reload successfully finished")
      cmd_message:delete()
      return
    else
      noAccessMessage(cmd_message)
      return
    end
  end

  if commands[cmd] == nil or commands[cmd].func == nil then return end
  if commands[cmd][serverdata[cmd_message.server.id].lang] == nil and not commands[cmd].beta then
    writeLog("Command "..cmd.." does not support lang "..serverdata[cmd_message.server.id].lang..", server "..cmd_message.server.name.." (ID "..cmd_message.server.id..")")
    return
  end
  if commands[cmd].disabled == true then
    writeLog("Command "..cmd.." disabled")
    writeLog(string.format("Additional data: author: %s (ID %s), server: %s (ID %s), message content: %s", cmd_message.author.name, cmd_message.author.id, cmd_message.server.name, cmd_message.server.id, cmd_message.content))
    return
  end

  if not commands[cmd].group then
    commands[cmd].group = "user"
  end

  if commands[cmd].group == "admin" then
    if not checkPerm(cmd_message.author, cmd_message.server) then noAccessMessage(cmd_message);return end
  elseif commands[cmd].group == "owner" then
    if cmd_message.author.id ~= config.authorID then noAccessMessage(cmd_message);return end
  elseif commands[cmd].group == "permission" then
    if commands[cmd].perm then
      if not hasPermission(cmd_message.author, commands[cmd].perm) then noAccessMessage(cmd_message);return end
    else
      writeLog("No permission set for command "..cmd, "ERR")
      return
    end
  end

  if commands[cmd].whitelist then
    local answer = false
    for _, servName in pairs(commands[cmd].whitelist) do
      if servers[cmd_message.server.id] == servName then answer = true;break end
    end
    if answer == false then return end
  end

  if commands[cmd].blacklist then
    for _, servName in pairs(commands[cmd].blacklist) do
      if servers[cmd_message.server.id] == servName then return end
    end
  end

  if commands[cmd].members then
    local answer = false
    for _, membName in pairs(commands[cmd].members) do
      if cmd_message.author.id == membName then answer = true;break end
    end
    if answer == false then noAccessMessage(cmd_message);return end
  end

  if commands[cmd].split == nil then
    if commands[cmd].group == "user" then
      commands[cmd].split = true
    else
      commands[cmd].split = false
    end
  end

  if commands[cmd].split == true then
    arguments = {}
    for word in string.gmatch(cmd_message.content, "[^ ]+") do
      table.insert(arguments, word)
    end
  elseif type(commands[cmd].split) == type(2) then
    if commands[cmd].split > 1 then
      arguments = {}
      local iterations = 0
      local txt = cmd_message.content
      for word in string.gmatch(cmd_message.content, "[^ ]+") do
        iterations=iterations+1
        if iterations==commands[cmd].split then
          table.insert(arguments, txt)
          break
        else
          table.insert(arguments, word)
          txt = txt:match("[^ ]+ (.*)")
        end
      end
    end
  end

  if not checkPerm(cmd_message.author, cmd_message.server) and not commands[cmd].allowMultiple then
    previousCommand[cmd_message.server.id][cmd_message.author.id] = cmd_message.content
  end

  local dontDeleteMessage = commands[cmd].func(cmd_message, arguments)
  writeLog(string.format("Command %s triggered. Additional data: author: %s (ID %s), server: %s (ID %s), message content: %s", cmd, cmd_message.author.name, cmd_message.author.id, cmd_message.server.name, cmd_message.server.id, cmd_message.content))
  if dontDeleteMessage ~= true and serverdata[cmd_message.server.id].deleteBotMessages == true then cmd_message:delete() end
end)

client:run(config.token)