# Answernator T-800 #
# We moved to [Gitlab.ely.by](https://gitlab.ely.by/mcmodder/oc-progs) #
## Установка ##
* Установите Luvit. Для этого посетите [luvit.io](https://luvit.io) и следуйте инструкциям по установке.
* Установите библиотеку Discordia, выполнив `lit install SinisterRectus/discordia`
* Запустите бота командой `luvit bot.lua`

## Настройка ##
После первого запуска бота в папке с ним появится файл **`bot.cfg`**
Описание настроек в нем:

* `authorID` - ID хозяина бота
* `token` - токен бота. Без него он тупо не залогинится
* `commandSymbol` - символ, с которого будет начинаться команда. По идее, сделано так, что может поддерживать и несколько символов
* `adminRoleName` - имя роли, на владельцев которой бот будет реагировать, как на своих админов (группа команд **admin**)
* `whitelistRoleName` - имя роли, владельцев которой бот никогда не сможет кикнуть
* `removeDisabledCommands` - если `true`, то отключенные команды будут удаляться при запуске (`command[commandName] = nil`)
* `botID` -  ID бота (*запрашиваю т.к. бот узнает свой ID уже после загрузки всех текстов*)
* `defaultLang` - стандарнтный язык бота для всех новых серверов
* `deleteDelay` - задержка перед удалением сообщений бота

Остальное доступно из бота командами /help и/или /command help (если это предусмотрено)

# EN: #
## Installing ##
* Install Luvit. Check instructions on [luvit.io](https://luvit.io).
* Install Discordia: `lit install SinisterRectus/discordia`
* Run bot: `luvit bot.lua`

## Settings: ##
Config file **`bot.cfg`** will appear after first bot run.
You can see this settings there:

* `authorID` - bot owner's ID
* `token` - bot token
* `commandSymbol` - command symbol, for example `!` or `/`
* `adminRoleName` - bot admin role name (**admin** commands)
* `whitelistRoleName` - this role owners' bot newer can kick
* `removeDisabledCommands` - if `true`, bot will remove all disabled commands on startup (`command[commandName] = nil`)
* `botID` - bot account ID (*he needs it to generate some help texts on startup*)
* `defaultLang` - default bot language for all new servers
* `deleteDelay` - bot's messages deleting delay

You can see some other help using `!help` or `![command] help`.
Don't forget to change bot language via `!lang [lang]`!
Don't forget to replace ! in commands above if you changed command symbol in settings!
